//
//  ViewController.m
//  Task2
//
//  Created by admin on 17/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"
@import MobileCoreServices;

@interface ViewController ()

@end
@implementation ViewController{
    
    UIImage *image;
    UIImageView *imageView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(100, 200, 200, 100)];
    button.backgroundColor = [UIColor whiteColor];
    [button setTitle:@"Click me......." forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
 

    
}

-(void)buttonClicked{
    imageView.image = nil;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert!!!" message:@"Select:" preferredStyle:UIAlertControllerStyleActionSheet];

        
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Like" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        image = [UIImage imageNamed:@"like.png"];
        imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(20, 350, 200, 200);
        [self.view addSubview:imageView];
        
        
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Unlike" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        image = [UIImage imageNamed:@"unlike.png"];
        imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake(20, 350, 200, 200);
        [self.view addSubview:imageView];

        
         }];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    [alert addAction:action1];
    [alert addAction:action2];
    [alert addAction:action3];
    
    [self presentViewController: alert animated:YES completion:^{
        
    }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
